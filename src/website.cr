require "stout"
include Stout::Magic

server = Stout::Server.new

server.get("/") do |context|
  context << ecrs("#{__DIR__}/template/index.ecr")
end

server.listen
